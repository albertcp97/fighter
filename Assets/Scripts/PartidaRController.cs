﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartidaRController : MonoBehaviour
{
    public Text time;
    public float timeA;
    public float count;
    public float count2;
    public float timeB;
    public GameObject[] shoot;
    private bool bola;
    public bool fin;


    //LO MISMO QUE EL PARTIDACONTROLLER PERO PARA LA ESCENA 3 PARA CONTROLAR LA GENERACION DE PROYECTILES
    void Start()
    {
        timeA = 99;
        count = 0;
        count2 = 0;
        timeB = 0;
        bola = false;
        fin = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (timeA > 0)
        {
            timeA -= Time.deltaTime;
        }
        else
        {
            fin = true;
        }
         
        if (!bola)
        {
            
            GameObject newShoott = Instantiate(shoot[0]);
            newShoott.transform.SetParent(GameObject.Find("balas").transform);
            newShoott.transform.position = new Vector2(
                   UnityEngine.Random.Range(-7f,7f),
                  20f

               );
            Debug.Log(newShoott.transform.position.x+" "+ newShoott.transform.position.y);
            StartCoroutine(cool());
        }
       
       
        
        time.text = timeA.ToString("f0");



    }

    private IEnumerator cool()
    {
        bola = true;
        yield return new WaitForSecondsRealtime(2f);
        bola = false;


    }
}
