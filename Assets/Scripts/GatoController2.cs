﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatoController2 : MonoBehaviour
{
    // Start is called before the first frame update
    //Disparo de personaje2
    void Start()
    {//asignar posicion y añadiorle una fuerza
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200, -500));
        this.transform.position = new Vector2(
                  GameObject.Find("Player2").transform.position.x ,
                  GameObject.Find("Player2").transform.position.y+3

              );
    }

    // Update is called once per frame
    void Update()
    {//se ira haciendo grande con el tiempo
        this.GetComponent<Transform>().localScale = new Vector3(this.transform.localScale.x + 0.01f, this.transform.localScale.y + 0.01f, this.transform.localScale.z);
        
    }
    //si toca al personaje rival se destruye
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            Debug.Log("Holap");
            Destroy(this.gameObject);
        }

    }
}
