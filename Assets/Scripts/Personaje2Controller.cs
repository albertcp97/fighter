﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personaje2Controller : MonoBehaviour
{
    public int Vida;
    private int vidam;
    public delegate void _OnDamanged2(int VidaM, int vida);
    public static event _OnDamanged2 OnDamanged;
    
    private int stat;
    private bool cancelMov;
    private bool recibirD;
    private bool ensalto;
    public bool fin;
    public GameObject[] shoot;
    public bool fin1;
    public PartidaRController fin2;
    public GameObject p;
    public Text txt;
    public GameObject[] ataques;
    public int atq;
    // Start is called before the first frame update
    void Start()
    {
      
        stat = 0;
        cancelMov = true;
        Vida = 100;
        ensalto = false;
        vidam = Vida;
        recibirD = false;
       
        fin1 = false;
        
        StartCoroutine(inmune());
    }

    private IEnumerator inmune()
    {
        yield return new WaitForSecondsRealtime(2f);
        recibirD = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x <= p.transform.position.x)
        {
            StartCoroutine(CanMove2());

            
        }
        if (this.transform.position.y < -5)
        {
            fin1 = true;
            txt.text = "Guanya Personaje1";

        }
        if (Vida <= 0)
        {
            fin1 = true;
            txt.text = "Guanya Personaje1";

        }
        if (fin1)
        {
            Debug.Log("holi");
            cancelMov = false;
            this.GetComponent<Animator>().SetBool("mover", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
            this.GetComponent<Animator>().SetBool("cubrirMedio", false); this.GetComponent<Animator>().SetBool("cubrirBajo", false);
            this.GetComponent<Animator>().SetBool("Muerto", true);
            


        }
        if (cancelMov)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {


                if (!this.GetComponent<Animator>().GetBool("agachate"))
                {
                    if (!this.GetComponent<Animator>().GetBool("cubrirMedio"))
                    {
                        this.transform.Translate(0.04f, 0, 0);
                    }

                }

                this.GetComponent<Animator>().SetBool("mover", true);

                this.GetComponent<SpriteRenderer>().flipX = false;
                this.GetComponent<Animator>().SetBool("cubrirMedio", false);
                this.GetComponent<Animator>().SetBool("agachate", false);
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    this.GetComponent<Animator>().SetBool("cubrirMedio", false);
                    if (!this.GetComponent<Animator>().GetBool("agachate"))
                    {
                        transform.Translate(-0.04f, 0, 0);
                    }

                    this.GetComponent<SpriteRenderer>().flipX = true;

                    this.GetComponent<Animator>().SetBool("mover", true);

                    this.GetComponent<Animator>().SetBool("agachate", false);
                }

                else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
                {


                    if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        this.GetComponent<Animator>().SetBool("agachate", true);
                        this.GetComponent<Animator>().SetBool("Salto", false);
                        this.GetComponent<Animator>().SetBool("mover", false);

                    }

                    if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (!ensalto)
                        {
                            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1000));
                            this.GetComponent<Animator>().SetBool("Salto", true);
                            ensalto = true;
                        }


                    }
                }
                else
                {
                    this.GetComponent<Animator>().SetBool("mover", false);
                    this.GetComponent<Animator>().SetBool("agachate", false);
                    this.GetComponent<Animator>().SetBool("cubrirMedio", false);
                }


            }
            
                       
            
          
                if (Input.GetKeyDown("j"))
                {

                    if (this.GetComponent<Animator>().GetBool("Salto"))
                    {
                        this.GetComponent<Animator>().SetBool("ataqueSalto", true);
                    }
                    else { StartCoroutine(CoolDown()); }



                }
            
            if (Input.GetKey("k"))
            {
                if (this.GetComponent<Animator>().GetBool("agachate"))
                {

                }
                else
                {
                    this.GetComponent<Animator>().SetBool("cubrirMedio", true);
                    this.GetComponent<Animator>().SetBool("mover", false);
                }


            }


            if (Input.GetKeyDown("l"))
            {
                if (this.GetComponent<Animator>().GetBool("agachate"))
                {

                }
                else
                {
                    if (!this.GetComponent<Animator>().GetBool("Salto"))
                    {
                        if (!this.GetComponent<Animator>().GetBool("ataqueBase"))
                        {
                            this.GetComponent<Animator>().SetBool("ataqueBase", true);

                            StartCoroutine(coolBase());
                            GameObject newShoott = Instantiate(shoot[0]);
                            newShoott.transform.SetParent(GameObject.Find("Playe2").transform);

                        }
                    }
                }

            }
            if (this.GetComponent<Animator>().GetBool("cubrirBajo") || this.GetComponent<Animator>().GetBool("cubrirMedio"))
            {
                recibirD = true;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
            }
            else
            {
                recibirD = false;
                this.GetComponent<BoxCollider2D>().isTrigger = false;

            }

            //Debug.Log(stat);

            switch (stat)
            {
                case 0:
                    {


                    }
                    break;
                case 1:
                    {

                        this.GetComponent<Animator>().SetBool("ataqueBase", true);

                        StartCoroutine(coolBase());

                    }
                    break;
                case 2:
                    {
                        this.GetComponent<Animator>().SetBool("ataqueBase", true);
                        this.GetComponent<Animator>().SetBool("ataqueBase2", true);

                        StartCoroutine(coolBase2());
                    }
                    break;
                case 3:
                    {
                        this.GetComponent<Animator>().SetBool("ataqueBase", true);
                        this.GetComponent<Animator>().SetBool("ataqueBase2", true);
                        this.GetComponent<Animator>().SetBool("ataqueBase3", true);
                        StartCoroutine(coolBase3());

                    }
                    break;
            }
        }
    }

    private IEnumerator coolBase3()
    {

        yield return new WaitForSecondsRealtime(0.5f);
        this.GetComponent<Animator>().SetBool("ataqueBase3", false);
        stat = 0;
    }

    private IEnumerator coolBase2()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        this.GetComponent<Animator>().SetBool("ataqueBase2", false);
        stat = 0;
    }

    private IEnumerator coolBase()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        stat = 0;
        this.GetComponent<Animator>().SetBool("ataqueBase", false);
    }

  

   

    public IEnumerator CoolDown()
    {
        if (stat == 3)
        {
            stat = 1;

        }
        else { stat++; }
        yield return new WaitForSecondsRealtime(0.05f);






        /*
         yield return new WaitForSecondsRealtime(0.5f);
         this.GetComponent<Animator>().SetBool("ataqueBase2", false);
         this.GetComponent<Animator>().SetBool("ataqueBase", false);
         this.GetComponent<Animator>().SetBool("ataqueBase3", false);
         this.GetComponent<Animator>().SetBool("atacando", false);
         cool = true;*/

    }

    public void OnEnable()
    {
       ataqueController.OnDamang += HandleOnDmg;
        PartidaController.finp += Handlefinp;
    }
    public void OnDisable()
    {
        ataqueController.OnDamang -= HandleOnDmg;
        PartidaController.finp -= Handlefinp;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (ensalto)
        {


            if (collision.gameObject.name == "Pista")
            {

                this.GetComponent<Animator>().SetBool("mover", false);
                this.GetComponent<Animator>().SetBool("agachate", false);

            }
        }
       
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (ensalto)
        {

            if (collision.gameObject.name == "Pista")
            {

                this.GetComponent<Animator>().SetBool("mover", false);
                this.GetComponent<Animator>().SetBool("agachate", false);
                this.GetComponent<Animator>().SetBool("Salto", false);
                this.GetComponent<Animator>().SetBool("ataqueSalto", false);
                ensalto = false;
            }
        }
       
        if (collision.gameObject.tag == "disparo1")
        {
            if (!recibirD)
            {
                Debug.Log(Vida + " " + vidam);
                Vida -= 10*atq;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300, 300));
                OnDamanged?.Invoke(vidam, Vida);
                StartCoroutine(CanMove2());
            }
        }

        if (collision.gameObject.tag == "bala")
        {
            Vida -= 10;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300, 300));
            OnDamanged?.Invoke(vidam, Vida);
            StartCoroutine(CanMove2());
        }
        if (collision.gameObject.name == "buff")
        {
            GameObject a = GameObject.Find("buf");
            Destroy(a);
            Debug.Log("buff");
            atq++;
           
        }
    }
    public void HandleOnDmg(int v)
    {Debug.Log(Vida + " " + vidam);
        if (!recibirD) { 
        
        Vida -= v*atq;
        if (v > 10)
        {
                OnDamanged?.Invoke(vidam, Vida); StartCoroutine(CanMove());
            }
            else
            {
                OnDamanged?.Invoke(vidam, Vida);StartCoroutine(CanMove2());
            }
            
            
        }


    }

    private IEnumerator CanMove2()
    {
        recibirD = false;
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(700, 100));
        this.GetComponent<Animator>().SetBool("golpeado", true);
        yield return new WaitForSecondsRealtime(1.5f);
        recibirD = true;
        this.GetComponent<Animator>().SetBool("golpeado", false);
        cancelMov = true;
    }

    private IEnumerator CanMove()
    {
        this.GetComponent<Animator>().SetBool("golpeado", true);
        yield return new WaitForSecondsRealtime(1f);
        this.GetComponent<Animator>().SetBool("golpeado", false);
        cancelMov = true;
    }

    public void Handlefinp(bool fin)
    {
        fin1 = true;
    }
}



