﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersanajeController : MonoBehaviour
{
    // Start is called before the first frame update
   
    private int stat;//INDICA EN QUE ATAQUE DEL COMBO ESTA
    private bool cancelMov;//SI SE PUEDE MOVER O NO

    public int Vida;//VIDA ACTUAL
    private int vidam;//VIDA MAXIMA
    private bool ensalto;//SI ESTA EN EL AIRE
    public delegate void _OnDamanged2(int VidaM, int vida);//DELEGADO QUE CONTROLA LA BARRA DE VIDA
    public static event _OnDamanged2 OnDamanged2;
    public int atq;//DIVISOR DE ATAQUE QUE RECIBES(DAÑO QUE TE HACEN)
    private bool recibirD;//SI RECIBE DAÑO O NO
    public bool fin1; //FINALIZAR COMBATE

    public GameObject p;//PERSONAJE2
    public GameObject[] shoot;//DISPAROS QUE TIENE EL PERSONAJE
    public Text txt;//TEXTO EN PANTALLA DE VICTORIA
    void Start()
    {
       //ESTE PERSONAJE ES MAS LIGERO, SALTA MAS Y AGUANTA MENOS
        stat = 0;
        cancelMov = true;
        Vida = 100;
        ensalto = false;
        vidam = Vida;
        recibirD = false;
        atq = 2;
        fin1 = false;
        StartCoroutine(inmune());
        //INICIALIZAMOS TODO LOS PARAMETROS Y HACEMOS AL PERSONAJE INMUNE DURANTE UN TIEMPO(evitar errores)
    }

    private IEnumerator inmune()
    {
        yield return new WaitForSecondsRealtime(2f);
        recibirD = true;//PASADO EL TIEMPO HACE AL PERSONAJE VULNERABLE
    }

    // Update is called once per frame
    void Update()
    {//SI CAE MUERE, PARA RESUMIR
        if (this.transform.position.y < -5)
        {
            fin1 = true;
            txt.text = "Guanya Personaje2";

        }
        //EVITO QUE ALGUNO DE LOS PERSONAJES SALTE AL OTRO POR ENCIMA
        if (this.transform.position.x >= p.transform.position.x)
        {
            this.GetComponent<Animator>().SetBool("mover", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
            this.GetComponent<Animator>().SetBool("cubrirMedio", false); this.GetComponent<Animator>().SetBool("cubrirBajo", false);
            StartCoroutine(CanMove2());


        }
        //SI NO TIENE VIDA MUERE
        if (Vida <= 0)
        {
            fin1 = true;
            txt.text = "Guanya Personaje2";

        }
       //SI PASA EL TIEMPO LOS DOS PERSONAJES SE QUEDAN INMOVILES, POR FALTA DE TIEMPO NO HE PODIDO COMPROBAR QUIEN TENIA MAS VIDA
        if (fin1 )
        {
            cancelMov = false;
            this.GetComponent<Animator>().SetBool("mover", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
            this.GetComponent<Animator>().SetBool("cubrirMedio", false); this.GetComponent<Animator>().SetBool("cubrirBajo", false);

            this.GetComponent<Animator>().SetBool("Muerto", true);
            
           

        }
        //SI SE PUEDE MOVER ENTRARA A LOS IFS QUE COMPRUEBAN LAS TECLAS PARA HACER ACCIONES Y DEPENDE EL ESTADO EN EL QUE ESTE O TECLA PRESIONADA CAMBIARA DE ANIACIOM
        if (cancelMov) { 
        //MOVER DERECHA
        if (Input.GetKey(KeyCode.D))
        {

                //EVITAR DESPLAZAMIENTO SI ESTA AGACHADO
            if (!this.GetComponent<Animator>().GetBool("agachate")) {
                if (!this.GetComponent<Animator>().GetBool("cubrirMedio"))
                {
                    this.transform.Translate(0.04f, 0, 0);
                }
                
            }
                
                this.GetComponent<Animator>().SetBool("mover", true);
              
                this.GetComponent<SpriteRenderer>().flipX = false;
                this.GetComponent<Animator>().SetBool("cubrirMedio", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
        }
        else {
                //MOVER IZQUIERDA, LO MISMO QUE LA DERECHA
            if (Input.GetKey("a"))
            {this.GetComponent<Animator>().SetBool("cubrirMedio", false);
                if (!this.GetComponent<Animator>().GetBool("agachate"))
                {
                    transform.Translate(-0.04f, 0, 0);
                }

                this.GetComponent<SpriteRenderer>().flipX = true;
                
                this.GetComponent<Animator>().SetBool("mover", true);
                
                this.GetComponent<Animator>().SetBool("agachate", false);
            }
            //PARA SABER SI SE PUEDE AGACHAR O SALTAR
            else if(Input.GetKey("s")|| Input.GetKey("w"))
            {


                    if (Input.GetKeyDown("s"))
                    {
                        this.GetComponent<Animator>().SetBool("agachate", true);
                        this.GetComponent<Animator>().SetBool("Salto", false);
                        this.GetComponent<Animator>().SetBool("mover", false);

                    }
            
                if (Input.GetKeyDown("w"))
                {
                        if (!ensalto) { 
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1000));
                        this.GetComponent<Animator>().SetBool("Salto", true);
                            ensalto = true;
                        }


                    }
                }
                else
            {//SINO, VUELVE AL ESTADO 0 (QUIETO)
                this.GetComponent<Animator>().SetBool("mover", false);
                this.GetComponent<Animator>().SetBool("agachate", false);
                this.GetComponent<Animator>().SetBool("cubrirMedio", false); this.GetComponent<Animator>().SetBool("cubrirBajo", false);
                }


        }
       
                //ATAQUE DEL PERSONAJE, COMPRUEBA SI ESTA AGACHAO O EN SALTO O EN EL SUELO Y DEPENDE QUE HARA UNA ACCION O OTRA. SI ESTA EN EL SUELO EMPEZARA CORUTINA QUE PERMITE LOS COMBOS
                if (Input.GetKeyDown("y"))
            {
                if (this.GetComponent<Animator>().GetBool("agachate"))
                {
                    this.GetComponent<Animator>().SetBool("ataqueBajo", true);

                }
                if (this.GetComponent<Animator>().GetBool("Salto"))
                {
                    this.GetComponent<Animator>().SetBool("ataqueSalto", true);
                }
                else {StartCoroutine(CoolDown()); }

                    

            }
                //TECLA PARA CUBRIRSE DE ATAQUES SI ESTA AGACHADO CUBRIRA POR ABAJO, SI ESTA EN POSICION NORMAL CUBRIRA ARRIBA
            if (Input.GetKey("t"))
            {
                if (this.GetComponent<Animator>().GetBool("agachate"))
                {
                    this.GetComponent<Animator>().SetBool("cubrirBajo", true);
                    this.GetComponent<Animator>().SetBool("mover", false);
                }
                else
                {
                    this.GetComponent<Animator>().SetBool("cubrirMedio", true);
                    this.GetComponent<Animator>().SetBool("mover", false);
                }


            }//DESCATIVAMOS EL CUBRIR
            else { this.GetComponent<Animator>().SetBool("cubrirMedio", false); this.GetComponent<Animator>().SetBool("cubrirBajo", false); }
           
            //TELCA PARA EL DISPARO, COMPRUEBO SI NO ESTA AGACHADO O EN SALTO, SI NO LO ESTA DISPARARA UN GATO POCHO QUE HARA UNA PARABOLA, 
            //ESTE TIENE ELASTICIDAD, POR LO CUAL IRA REBOTANDO(COMO LAS PARTIDAS NO DURAN MUCHO NO ELIMINO LOS PROYECTILES POR TIEMPO, SOLO SI IMPACTAN EN EL OTRO JUGADOR )
            if(Input.GetKeyDown("r"))
            {
                if (this.GetComponent<Animator>().GetBool("agachate"))
                {

                }
                else {
                    if (!this.GetComponent<Animator>().GetBool("Salto")) {
                        //SI ESTA EN ATAQUE BASE, NO VUELVE A DISPARAR(ES UN COOLDOWN)
                        if (!this.GetComponent<Animator>().GetBool("ataqueBase")) { 
                        this.GetComponent<Animator>().SetBool("ataqueBase", true);
                            //PARA DESHABILITAR EL ATAQUE BASE
                        StartCoroutine(coolBase());
                        GameObject newShoot = Instantiate(shoot[0]);
                        newShoot.transform.SetParent(GameObject.Find("DisparoGatuno").transform);

                        }
                    }
                }

            }
            //SI EL PERSONAJE ESTA CUBIERTO SE VUELVE TRIGGER Y NO RECIBE DAÑO(TENIA IMPLEMENTADO BOX COLLIDER DE CUBRIRSE, PERO NO HE SABIDO ACCEDER A ESTOS DESDE EL OTRO PERSONAJE)
            if (this.GetComponent<Animator>().GetBool("cubrirBajo")|| this.GetComponent<Animator>().GetBool("cubrirMedio"))
            {
                recibirD = true;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
            }
            else
            {
                recibirD = false; this.GetComponent<BoxCollider2D>().isTrigger = false;

            }

           

            //ESTADO DEL COMBO, ESTADO 1 ATAQUE BASE 1, 2 ATAQUE BASE 2, 3 ATAQUE BASE 3. SI NO PARAS DE PRESIONAR LA TECLA ATAQUE SE BUGUEA, HACE UN COMBO Y LUEGO NO PASA DEL ATAQUE 1, HASTA QUE NO DEJES DE PRESIONAR LA TECLA ATAQUE, LUEGO PODRAS HACER COMBO OTRA VEZ DE MANERA NORMAL(SUPONGO QUE ES POR UNITY)
            switch (stat)
        {
            case 0:
                {
                    
                    
                }
                break;
            case 1:
                {
                    
                    this.GetComponent<Animator>().SetBool("ataqueBase", true);
                   //QUITAR LA ACCION DE ATAQUE1
                    StartCoroutine(coolBase());

                }
                break;
            case 2:
                {
                    this.GetComponent<Animator>().SetBool("ataqueBase", true);
                    this.GetComponent<Animator>().SetBool("ataqueBase2", true);
                        //QUITAR LA ACCION DE ATAQUE2
                        StartCoroutine(coolBase2());
                }
                break;
            case 3:
                    {
                        this.GetComponent<Animator>().SetBool("ataqueBase", true);
                    this.GetComponent<Animator>().SetBool("ataqueBase2", true);
                    this.GetComponent<Animator>().SetBool("ataqueBase3", true);
                        //QUITAR LA ACCION DE ATAQUE3
                    StartCoroutine(coolBase3());

                }
                break;
        }
        }
    }

    private IEnumerator coolBase3()
    {
        
        yield return new WaitForSecondsRealtime(0.5f);
        this.GetComponent<Animator>().SetBool("ataqueBase3", false);
        //SI DESPUES DEL TIEMPO NO SE HA ACCIONADO TECLA ATAQUE, VUELVE AL ESTADO 0. LO MISMO PARA LOS DEMAS
        stat = 0;
    }

    private IEnumerator coolBase2()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        this.GetComponent<Animator>().SetBool("ataqueBase2", false);
        stat = 0;
    }

    private IEnumerator coolBase()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        stat = 0;
        this.GetComponent<Animator>().SetBool("ataqueBase", false);
    }

   
    //CAMBIO DE ESTADO AL ATACAR, SI LLEGA ESTADO 3, VUELVE AL 1
    public IEnumerator CoolDown()
    {
        if (stat == 3)
        {
            stat = 1;
            
        }
        else {  stat++; }
        yield return new WaitForSecondsRealtime(0.05f);
        

        



        /*
         yield return new WaitForSecondsRealtime(0.5f);
         this.GetComponent<Animator>().SetBool("ataqueBase2", false);
         this.GetComponent<Animator>().SetBool("ataqueBase", false);
         this.GetComponent<Animator>().SetBool("ataqueBase3", false);
         this.GetComponent<Animator>().SetBool("atacando", false);
         cool = true;*/
       
    }

    

    private void OnCollisionExit2D(Collision2D collision)
    {//SI esta en el suelo y saltas, desactivas los movimientos de mover y agacharte(evitar que cambie de animator en el aire)
        if (ensalto) {

           
            if (collision.gameObject.name == "Pista")
        {
       
            this.GetComponent<Animator>().SetBool("mover", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
           
        }
        }
    }
    //control de delegados(uno de la vida y otro de fin tiempo)
    public void OnEnable()
    {Debug.Log(Vida + " " + vidam);
        ataqueController2.OnDmg += HandleOnDamaged;
        PartidaController.finp += Handlefinp;

    }
    public void OnDisable()
    {
        ataqueController2.OnDmg -= HandleOnDamaged;
        PartidaController.finp -= Handlefinp;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //SI ESTA EN EL AIRE MIRA CUANDO TOCA SUELO PARA PODER MOVERTE OTRA VEZ Y DESACTIVAR SALTO
        if (ensalto)
        {
           
            if (collision.gameObject.name == "Pista")
            {
                
                this.GetComponent<Animator>().SetBool("mover", false);
                this.GetComponent<Animator>().SetBool("agachate", false);
                this.GetComponent<Animator>().SetBool("Salto", false);
                this.GetComponent<Animator>().SetBool("ataqueSalto", false);
                ensalto = false;
            }
        }
        //si chocas con el disparo del otro player, recibes daño. Esta controla la vida del personaje se reduzca
        if (collision.gameObject.tag == "disparo2")
        {
            if (!recibirD)
            {//se desplazara hacia atras al recibir el golpe
                Debug.Log("Box");
                Vida -= 10*atq;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300, 100));
                OnDamanged2?.Invoke(vidam, Vida);
                StartCoroutine(CanMove2());
            }
        }
        //para la escena 3, lo mismo que la de disparo
        if (collision.gameObject.tag == "bala")
        {
            Vida -= 10*atq;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300, 300));
            OnDamanged2?.Invoke(vidam, Vida);
            StartCoroutine(CanMove2());
        }
        //para escena 2 si coges el centimo te vueles mas duro
        if (collision.gameObject.name == "buff")
        { GameObject a = GameObject.Find("buf");
            Destroy(a);
            atq/=2;

        }
        /*if (collision.gameObject.tag == "cat")
        {
            cancelMov = false;
            
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.GetComponent<Animator>().SetBool("mover", false);
            this.GetComponent<Animator>().SetBool("agachate", false);
            this.GetComponent<Animator>().SetBool("Salto", false);
            this.GetComponent<Animator>().SetBool("ataqueSalto", false);

            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 200));
            
            Vida -= 10;
            
            if (Vida <= 0)
            {
                cancelMov = false;
                

            }
            else {
                Debug.Log(Vida + " " + vidam);
                OnDamanged2?.Invoke(vidam, Vida);this.GetComponent<Animator>().SetBool("Muerto", true);
            }
            StartCoroutine(CanMove());*/

    }
    //al recibir daño reduces barra vida
    public void HandleOnDamaged(int v)
    {
        if (!recibirD)
        {
            Debug.Log(Vida + " " + vidam);

            Vida -= v*atq;

            if (v > 10)
            {
                OnDamanged2?.Invoke(vidam, Vida); StartCoroutine(CanMove());
            }
            else
            {
                OnDamanged2?.Invoke(vidam, Vida); StartCoroutine(CanMove2());
            }

        }




    }
    //controla que puedas moverte y activa el animator de golpeado
    private IEnumerator CanMove()
    {
        this.GetComponent<Animator>().SetBool("golpeado", true);
        
        yield return new WaitForSecondsRealtime(1.5f);
        this.GetComponent<Animator>().SetBool("golpeado", false);
        cancelMov = true;
    }
    //lo mismo que la de arriba pero con retroceso del personaje(si es un golpe mas fuerte saldra disparado)
    private IEnumerator CanMove2()
    {
        recibirD = false;
        this.GetComponent<Animator>().SetBool("golpeado", true);
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-600, 100));
        yield return new WaitForSecondsRealtime(1.5f);
        this.GetComponent<Animator>().SetBool("golpeado", false);
        recibirD = true;
        cancelMov = true;
    }

 //acabar partida por tiempo
    public void Handlefinp(bool fin)
    {
        fin1 = true;
    }
}

