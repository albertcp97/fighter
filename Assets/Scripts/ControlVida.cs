﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControlVida : MonoBehaviour
{
    public Image img;
   
    //APARTIR DE DELEGADOS CONTROLAMOS LA BARRA DE VIDA
    public void OnEnable()
    {
        PersanajeController.OnDamanged2 += HandleOnDamaged;
    }
    public void OnDisable()
    {
        PersanajeController.OnDamanged2 -= HandleOnDamaged;
    }
    public void HandleOnDamaged(int vidam,int vida)
    {
        if (vida > 0) { 
        decimal a = decimal.Divide(decimal.Parse(vida.ToString()), decimal.Parse(vidam.ToString()));
        Debug.Log(a);
        img.fillAmount = float.Parse(a.ToString());
        }
        else
        {
            img.fillAmount = 0;
        }
    }
   

   
}
