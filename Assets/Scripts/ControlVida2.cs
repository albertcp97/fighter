﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlVida2 : MonoBehaviour
{
    public Image img;
    // Start is called before the first frame update

    public void OnEnable()
    {
        Personaje2Controller.OnDamanged += HandleOnDamaged;
    }
    public void OnDisable()
    {
        Personaje2Controller.OnDamanged -= HandleOnDamaged;
    }
    public void HandleOnDamaged(int vidam, int vida)
    {
        if (vida > 0)
        {
            decimal a = decimal.Divide(decimal.Parse(vida.ToString()), decimal.Parse(vidam.ToString()));
            Debug.Log(a);
            img.fillAmount = float.Parse(a.ToString());
        }
        else
        {
            img.fillAmount = 0;
        }
    }
    
}
