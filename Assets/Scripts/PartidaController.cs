﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartidaController : MonoBehaviour
{
    public Text time;
    public float timeA;
    public float count;
    public float count2;
    public bool fin;
    public delegate void Fin(bool a);
    public static event Fin finp;
    // Start is called before the first frame update

        //CONTROL DE LA PARTIDA Y EL TIEMPO DE ESTA SI LLEGA A ZERO SE PARA LA PARTIDA
    void Start()
    {
        timeA = 99;
        count = 0;
        count2 = 0;
        fin = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeA > 0)
        {
            timeA -= Time.deltaTime;
        }
        else
        {
            finp?.Invoke(true);
            fin = true;
        }

        time.text = timeA.ToString("f0");
    }
}
